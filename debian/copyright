Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: acsccid
Upstream-Contact: Advanced Card Systems Ltd. <info@acs.com.hk>
Source: https://sourceforge.net/projects/acsccid/files/
Copyright: 2009-2024 Advanced Card Systems Ltd. <info@acs.com.hk>
License: LGPL-2.1+

Files: *
Copyright: 2009-2024 Advanced Card Systems Ltd. <info@acs.com.hk>
License: LGPL-2.1+

Files: config/config.rpath
Copyright: 1996-2020 Free Software Foundation, Inc.
License: public-domain

Files: debian/*
Copyright: 2011-2024 Godfrey Chung <godfrey.chung@acs.com.hk>
License: LGPL-2.1+

Files: MacOSX/*
Copyright: 1999-2011 Ludovic Rousseau <ludovic.rousseau@free.fr>
           1999-2005 David Corcoran <corcoran@musclecard.com>
           2003-2004 Damien Sauveron <damien.sauveron@labri.fr>
License: BSD-3-clause

Files: MacOSX/configure
Copyright: 2007-2009 Ludovic Rousseau <ludovic.rousseau@free.fr>
License: LGPL-2+

Files: MacOSX/convert_reader_h.pl
Copyright: 2008 Ludovic Rousseau <ludovic.rousseau@free.fr>
License: LGPL-2+

Files: src/*
Copyright: 2009-2019 Advanced Card Systems Ltd. <info@acs.com.hk>
           2003-2011 Ludovic Rousseau <ludovic.rousseau@free.fr>
           2005 Martin Paljak
License: LGPL-2.1+

Files: src/*.pl
Copyright: 2004-2009 Ludovic Rousseau <ludovic.rousseau@free.fr>
License: LGPL-2+

Files: src/misc.h
Copyright: 2005-2010 Ludovic Rousseau <ludovic.rousseau@free.fr>
License: BSD-3-clause

Files: src/parser.h
Copyright: 2003 Toni Andjelkovic <toni@soth.at>
           2005-2009 Ludovic Rousseau <ludovic.rousseau@free.fr>
License: BSD-3-clause

Files: src/simclist.*
Copyright: 2007-2011 Mij <mij@bitchx.it>
License: ISC

Files: src/strlcpy.c
Copyright: 1998 Todd C. Miller <Todd.Miller@courtesan.com>
License: ISC

Files: src/strlcpycat.h
Copyright: 2004-2010 Ludovic Rousseau <ludovic.rousseau@free.fr>
License: BSD-3-clause

Files: src/tokenparser.*
Copyright: 2001-2003 David Corcoran <corcoran@musclecard.com>
           2003-2010 Ludovic Rousseau <ludovic.rousseau@free.fr>
License: BSD-3-clause

Files: src/openct/*
Copyright: 2011 Advanced Card Systems Ltd. <info@acs.com.hk>
           2004 Ludovic Rousseau <ludovic.rousseau@free.fr>
           2003 Olaf Kirch <okir@suse.de>
           1999-2002 Matthias Bruestle
License: LGPL-2.1+

Files: src/towitoko/*
Copyright: 2011 Advanced Card Systems Ltd. <info@acs.com.hk>
           2000-2001 Carlos Prados <cprados@yahoo.com>
License: LGPL-2+

License: public-domain
 This file is free software; the Free Software Foundation
 gives unlimited permission to copy and/or distribute it,
 with or without modifications, as long as this notice is preserved.

License: ISC
 Permission to use, copy, modify, and distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the
 Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-2".

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-2.1".

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 Changes to this license can be made only by the copyright author with
 explicit written consent.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
